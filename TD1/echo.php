<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte;

          echo nl2br("\n");


          $coordonnees = array (
            'prenom' => 'François',
            'nom'    => 'Dupont'  );

          foreach ($coordonnees as $cle => $valeur){
              echo nl2br("$cle : $valeur\n");
          }

          echo nl2br("\n");

          $voiture = array (
            'marque' => 'Tesla',
            'couleur'    => 'bleu',
            'immatriculation' => 'AA1'  );

          $voiture1 = array (
            'marque' => 'Citroen',
            'couleur'    => 'Gris',
            'immatriculation' => 'B5A'  );

          $voiture2 = array (
            'marque' => 'Renault',
            'couleur'    => 'Blanc',
            'immatriculation' => '4K8'  );

          $voitures = array (
            1 => $voiture,
            2 => $voiture1,
            3 => $voiture2 );

          var_dump($voitures);

          foreach ($voitures as $cle => $valeur){
            echo "<h1>Liste des voitures</h1>";
            foreach ($valeur as $cle => $valur){
                echo nl2br("$cle : $valur\n");

          }
      }

      $testtab = array();
      if(empty($testtab))
      echo("Tableau vide");
 



        ?>
    </body>
</html> 