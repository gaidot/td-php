<?php
   
class Voiture {
   
    private $marque;
    private $couleur;
    private $immatriculation;
   
    // un getter      
    public function getMarque() {
        return $this->marque;
    }

    public function getCouleur() {
        return $this->couleur;
    }

    public function getImmatriculation() {
        return $this->immatriculation;
    }
   
    // un setter 
    public function setMarque($m) {
        $this->marque = $m;
    }
     
    public function setCouleur($c) {
        $this->couleur = $c;
    }

    public function setImmatriculation($i) {
        if($i<=8) {
            $this->immatriculation = $i;    
        }
    }
   
    // un constructeur
    public function __construct($m, $c, $i) {
        $this->marque = $m;
        $this->couleur = $c;
        $this->immatriculation = $i;
    } 
              
    // une methode d'affichage.
    public function afficher() {
        echo "Marque = $this->marque <br>";
        echo "Couleur =  $this->couleur <br>";
        echo "Immatriculation = $this->immatriculation";

    }
}
?>

